import { Base64 } from "js-base64";
import type { GitProvider } from "@tinacms/datalayer";
import { Gitlab } from "@gitbeaker/core";

type GitlabOptions = any; // TODO: import from gitbeaker

export interface GitlabProviderOptions {
  projectId: string;
  token: string;
  branch: string;
  commitMessage?: string;
  rootPath?: string;
  gitlabOptions?: GitlabOptions;
}

export class GitlabProvider implements GitProvider {
  gitlab: Gitlab;
  branch: string;
  projectId: string;
  rootPath?: string;
  commitMessage: string;

  constructor(args: GitlabProviderOptions) {
    this.projectId = args.projectId;
    this.branch = args.branch;
    this.commitMessage = args.commitMessage ?? "Edited with TinaCMS";
    this.rootPath = args.rootPath;
    this.gitlab = new Gitlab({
      token: args.token,
      ...(args.gitlabOptions || {}),
    });
  }

  async onPut(key: string, value: string) {
    const path = this.rootPath ? `${this.rootPath}/${key}` : key;

    try {
      const data = await this.gitlab.RepositoryFiles.edit(
        this.projectId,
        path,
        this.branch,
        value,
        this.commitMessage
      );

      console.log("Gitlab response: Edited file", data);
    } catch (e) {
      throw new Error(
        `Could not find file ${path} in repo ${this.projectId}/${this.projectId}`
      );
    }
  }

  async onDelete(key: string) {
    const path = this.rootPath ? `${this.rootPath}/${key}` : key;

    try {
      const data = await this.gitlab.RepositoryFiles.remove(
        this.projectId,
        path,
        this.branch,
        this.commitMessage
      );

      console.log("Gitlab response: Deleted file", data);
    } catch (e) {
      throw new Error(
        `Could not find file ${path} in repo ${this.projectId}/${this.projectId}`
      );
    }
  }
}
